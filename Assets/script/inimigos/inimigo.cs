﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inimigo : MonoBehaviour {
	public GameObject explosao;
	public float velocidadex;
	public float velocidadey;
	private int controlador = 0;
	public int esquerda = 0;
	public int direita = 0;
    public bool alvo = false;
	public Vector2 offset = new Vector2(0.5f,0.5f);
	public Vector3 userfulvalue = Vector2.zero;
	private Vector2 limit = Vector2.zero;
    public bool munition = false;
    public GameObject bonusmunition;
    public int valormunition = 10;

	public AudioClip SoundExplosion;

	private SoundManager sound;
	// Use this for initialization
	void Start () {
		limit = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, 0));	
		sound = SoundManager.instance;
	}	
	
	// Update is called once per frame
	void Update () {
		userfulvalue = (limit - offset);
		controlador++;
		if (controlador >= esquerda || transform.position.x > userfulvalue.x|| transform.position.x < -userfulvalue.x) {
			controlador = 0;
			velocidadex = velocidadex * -1;
		}
		if (transform.position.y > userfulvalue.y || transform.position.y < -userfulvalue.y) {
			velocidadey = velocidadey * -1;
		}

			transform.Translate (new Vector2(velocidadex*Time.deltaTime,-velocidadey*Time.deltaTime));
			
	}
	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("nave")||other.gameObject.CompareTag ("tiro_simples") || other.gameObject.CompareTag("missilnave") ) {
			Instantiate (explosao, transform.position, transform.rotation);

			Destroy (gameObject);
			sound.PlaySingle (SoundExplosion);
            if(munition == true) { 
                GameObject bonus = (GameObject)Instantiate(bonusmunition, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
                bonus.GetComponent<bonus>().valor = valormunition;
            }
        }
    }
}
