﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class misil : MonoBehaviour {
	public float speed = 5;
	public float velocidaderotacao = 200;
    private GameObject target;
    private GameObject [] targets;
    public GameObject explosao;
	public float tempodevida = 0f;
	public string inimigo ="nave";
	private float tempo = 0f;

	public AudioClip SoundExplosion;
	private SoundManager sound;

	Rigidbody2D rb;
	// Use this for initialization
	void Start ()  {
		
		sound = SoundManager.instance;

		if (inimigo != "nave")
        {
            targets = GameObject.FindGameObjectsWithTag(inimigo);
            foreach (GameObject tar in targets)
            {
                if (tar.GetComponent<inimigo>().alvo == false)
                {
                    tar.GetComponent<inimigo>().alvo = true;
                    target = tar;
                    break;
                }
                if (targets.Length != 0)
                {
                    tar.GetComponent<inimigo>().alvo = false;
                }
            }
        }
        else {
            target = GameObject.FindGameObjectWithTag(inimigo);
        }
        rb = GetComponent<Rigidbody2D> ();

	}
	
	// Update is called once per frame
	void Update () {
		//
		if (target != null) {
			Vector2	point2target = (Vector2)transform.position - (Vector2)target.transform.position;

			point2target.Normalize ();

			float value	= Vector3.Cross (point2target, transform.up).z;

			rb.angularVelocity = velocidaderotacao * value;

			rb.velocity = transform.up * speed;
			tempo += Time.deltaTime;
		} else {
			Vector2	point2target = (Vector2)transform.position - new Vector2(0,100);

			point2target.Normalize ();

			float value	= Vector3.Cross (point2target, transform.up).z;

			rb.angularVelocity = velocidaderotacao * value;

			rb.velocity = transform.up * speed;
			tempo += Time.deltaTime;
            target = GameObject.FindGameObjectWithTag(inimigo);
        }

		if (tempo >= tempodevida) {
			removeGameObject ();
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag (inimigo)||other.gameObject.CompareTag ("tiro_simples") ) {
			removeGameObject ();
		}
	}

	void removeGameObject(){
		sound.PlaySingle (SoundExplosion);
		Instantiate (explosao, transform.position, transform.rotation);
		Destroy (gameObject);
	}
}
