﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tiro : MonoBehaviour {
	public GameObject [] projetio;
	public float timetiro;
	public float time = 0;
	private int indiceprojetio=0;
    private GameObject [] cor; // [game1,game2,game3]
    public int qtdmunit2 = 10;
    private string munit;

	public AudioClip [] tirosound;

	private SoundManager sound;

	// Use this for initialization
	void Start () {
		time = timetiro;
        setprojetio("1");
        qtdmunit2 = 10;
		sound = SoundManager.instance;
    }
	
	// Update is called once per frame
	void Update () {
        GameObject.FindGameObjectWithTag("munit2").GetComponent<Text>().text = qtdmunit2.ToString();
        if(Input.inputString == "1" || Input.inputString == "2") { 
            setprojetio (Input.inputString);
        }
        if (Input.GetKey (KeyCode.Space)) {
			time += Time.deltaTime;
			if (time > timetiro) {
				
                if(munit == "1") {
					sound.RandomizeSfx (tirosound [0]);	
				    GameObject tiros = (GameObject)Instantiate (projetio[indiceprojetio], new Vector3 (transform.position.x, transform.position.y, transform.position.z), transform.rotation);
                }

                if (munit == "2" && qtdmunit2 != 0)
                {
					sound.RandomizeSfx (tirosound [1]);	
                    GameObject tiros = (GameObject)Instantiate(projetio[indiceprojetio], new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
                    qtdmunit2 -= 1;
                }

              

                    time = 0;
                // print(tiros.gameObject.tag);
            }
        }
        if (qtdmunit2 == 0)
        {
            setprojetio("1");
            setcolorred("tiro2");
        }
        if (qtdmunit2 > 0 && munit!= "2")
        {
            setcolorwhite("tiro2");
        }

    }
	void setprojetio(string op){
        munit = op;
		switch (op) 
		{
		case "1":
			indiceprojetio = 0;
            setcoloryellow("tiro1");
            setcolorwhite("tiro2");
            break;
		case "2":
            indiceprojetio = 1;
                if (qtdmunit2 != 0) { 
                    setcoloryellow("tiro2");
                    setcolorwhite("tiro1");
                }
                break;
		default:
			break;
		}
	}

    void setcoloryellow(string tag) {
        cor = GameObject.FindGameObjectsWithTag(tag);
        if (cor != null)
        {
            foreach(GameObject menutiro in cor)
            {
                menutiro.GetComponent<CanvasRenderer>().SetColor(Color.yellow);
            }
        }

    }

    void setcolorwhite(string tag) {
        cor = GameObject.FindGameObjectsWithTag(tag);
        if(cor != null)
        {
            foreach (GameObject menutiro in cor) {
                menutiro.GetComponent<CanvasRenderer>().SetColor(Color.white);
            }
        }

    }

    void setcolorred(string tag)
    {
        cor = GameObject.FindGameObjectsWithTag(tag);
        if (cor != null)
        {
            foreach (GameObject menutiro in cor)
            {
                menutiro.GetComponent<CanvasRenderer>().SetColor(Color.red);
            }
        }

    }
}
