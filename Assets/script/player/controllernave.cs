﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controllernave : MonoBehaviour {
	public float velocidade = 0;
	public Sprite [] sprites ;
    public Animator anim;
    private SpriteRenderer img;
	private int indice = 0;
    public float timedano;
    public float time = 0;
    public Vector2 offset = new Vector2(0.5f,0.5f);
	public Vector3 userfulvalue = Vector2.zero;
	private Vector2 limit = Vector2.zero;
	// Use this for initialization
	void Start () {
		img = GetComponent<SpriteRenderer> ();
		limit = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, 0));	
	}
	
	// Update is called once per frame
	void Update () {
		userfulvalue = (limit - offset);
       // anim.SetBool("dano", false);
        if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow)) {
			if (transform.position.y > -userfulvalue.y) {
				transform.position -= new Vector3 (0, (velocidade), 0) * Time.deltaTime;
			}
		}
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow)) {
			if (transform.position.y < userfulvalue.y) {
				transform.position += new Vector3 (0, (velocidade), 0) * Time.deltaTime;
			}
		}
		//se a tecla A estiver pressionada movimente a nave para esquerda e mude sua imagem
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
			if (transform.position.x > -userfulvalue.x) {
				indice = 0;
				transform.position -= new Vector3 ((velocidade), 0, 0) * Time.deltaTime;
			} else {
				indice = 1;
			}
	    //senão se a tecla D estiver pressionada movimente a nave para direita e mude sua imagem
		} else if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {
			if (transform.position.x < userfulvalue.x) {
				indice = 2;
				transform.position += new Vector3 ((velocidade), 0, 0) * Time.deltaTime;
			} else {
				indice = 1;
			}
		//senão  mude a imagem para nave parada
		} else {
			indice = 1;
		}

		img.sprite = sprites [indice];
        time += Time.deltaTime;
        if (time > timedano)
        {
            anim.SetBool("dano", false);
            
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("inimigo") || other.CompareTag("missil")) {
            anim.SetBool("dano",true);
            time = 0;
        }
    }
}
