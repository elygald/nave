﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projetio : MonoBehaviour {
	public float velocidade;
	// Use this for initialization
	void Start () {
		Destroy (gameObject, 2.5f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (new Vector3 (0, velocidade * Time.deltaTime, 0));
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("inimigo") || other.gameObject.CompareTag ("missil")) {
			Destroy (gameObject);
		}
	}
}
