﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonus : MonoBehaviour {

    public float velocidadex;
    public float velocidadey;
    private int controlador = 0;
    public int esquerda = 0;
    public int direita = 0;
    public int valor;
    private Vector2 limit = Vector2.zero;
    public Vector2 offset = new Vector2(0.5f, 0.5f);
    public Vector3 userfulvalue = Vector2.zero;
    // Use this for initialization
    void Start()
    {
        limit = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
    }

       // Update is called once per frame
    void Update()
    {
        userfulvalue = (limit - offset);
        controlador++;
        if (controlador >= esquerda || transform.position.x > userfulvalue.x || transform.position.x < -userfulvalue.x)
        {
            controlador = 0;
            velocidadex = velocidadex * -1;
        }
        if (transform.position.y > userfulvalue.y || transform.position.y < -userfulvalue.y)
        {
            velocidadey = velocidadey * -1;
        }
        transform.Translate(new Vector2(velocidadex * Time.deltaTime, -velocidadey * Time.deltaTime));
    }

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("nave")) {
            GameObject.FindGameObjectWithTag("armanave").GetComponent<tiro>().qtdmunit2 += valor;
            Destroy (gameObject);
		}
	}
}
